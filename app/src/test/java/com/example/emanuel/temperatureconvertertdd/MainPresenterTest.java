package com.example.emanuel.temperatureconvertertdd;

import com.example.emanuel.temperatureconvertertdd.app.exception.InvalidTemperatureValue;
import com.example.emanuel.temperatureconvertertdd.presenter.MainPresenter;
import com.example.emanuel.temperatureconvertertdd.presenter.MainPresenterImpl;

import junit.framework.TestCase;

import org.junit.Rule;
import org.junit.rules.ExpectedException;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class MainPresenterTest extends TestCase{

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private MainPresenter presenter;

    static Map<Float, Float> convertionList = new HashMap<Float,Float >();

    static {
        convertionList.put(0f,32f);
        convertionList.put(-200f,-328f);
        convertionList.put(95f,203f);
    }


    public MainPresenterTest() {
        this(MainPresenterTest.class.getName());
    }

    public MainPresenterTest(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        presenter = new MainPresenterImpl();
    }


    public void testToFahrenheit() throws InvalidTemperatureValue {
        for(Float key : convertionList.keySet()){
            float result = presenter.convertToFahrenheit(key);
            float expected = convertionList.get(key);
            assertEquals(expected, result);

        }
    }

    public void testToFahrenheitException()  {
        try {
            presenter.convertToFahrenheit(-273.151f);
            fail("Exception doesn't throw");
        } catch (InvalidTemperatureValue invalidTemperatureValue) {
            assertThat(invalidTemperatureValue.getMessage(), equalTo("you're violating the  thermodynamic's laws"));
        }
    }

    public void testToCelsiusException(){
        try {
            presenter.convertToCelsius(-459.6701f);
            fail("Exception doesn't throw");
        } catch (InvalidTemperatureValue invalidTemperatureValue) {
            assertThat(invalidTemperatureValue.getMessage(), equalTo("you're violating the  thermodynamic's laws"));
        }
    }


    public void testToCelsius() throws InvalidTemperatureValue {
        for(Map.Entry<Float,Float> entry : convertionList.entrySet()){
            float result = presenter.convertToCelsius(entry.getValue());
            float expected = entry.getKey();
            assertEquals(expected, result);

        }
    }
}