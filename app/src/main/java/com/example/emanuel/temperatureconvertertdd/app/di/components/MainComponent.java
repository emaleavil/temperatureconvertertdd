package com.example.emanuel.temperatureconvertertdd.app.di.components;

import com.example.emanuel.temperatureconvertertdd.app.activities.TemperatureConverterActivity;
import com.example.emanuel.temperatureconvertertdd.app.di.modules.MainModule;
import com.example.emanuel.temperatureconvertertdd.app.di.scopes.PerActivity;

import dagger.Component;

/**
 * Created by Emanuel on 23/12/2015.
 */
@PerActivity
@Component(dependencies = {ApplicationComponent.class}, modules = {MainModule.class})
public interface MainComponent {
    void inject(TemperatureConverterActivity mainActivity);
}