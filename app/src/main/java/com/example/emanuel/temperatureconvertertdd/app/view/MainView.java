package com.example.emanuel.temperatureconvertertdd.app.view;

/**
 * Created by emanuel on 5/01/16.
 */
public interface MainView {

    void setFahrenheit(String value);
    void setCelsius(String value);
    void showErrorMessage();
}
