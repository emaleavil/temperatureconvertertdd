package com.example.emanuel.temperatureconvertertdd.app.di.scopes;

import java.lang.annotation.Retention;

import javax.inject.Scope;


import static java.lang.annotation.RetentionPolicy.RUNTIME;
/**
 * Created by Emanuel on 23/12/2015.
 */
@Scope
@Retention(RUNTIME)
public @interface PerActivity {}
