package com.example.emanuel.temperatureconvertertdd.app;

import android.app.Application;

import com.example.emanuel.temperatureconvertertdd.app.di.components.ApplicationComponent;
import com.example.emanuel.temperatureconvertertdd.app.di.components.DaggerApplicationComponent;
import com.example.emanuel.temperatureconvertertdd.app.di.modules.ApplicationModule;

/**
 * Created by emanuel on 5/01/16.
 */
public class TemperatureConverterApp extends Application {

    private static TemperatureConverterApp sInstance = null;

    ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        component  = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
        component.inject(sInstance);
    }

    public ApplicationComponent getComponent(){
        return component;
    }

    public static TemperatureConverterApp getInstance(){
        return sInstance;
    }
}
