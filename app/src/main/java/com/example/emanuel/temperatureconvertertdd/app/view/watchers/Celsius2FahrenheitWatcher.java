package com.example.emanuel.temperatureconvertertdd.app.view.watchers;

import android.text.Editable;

import com.example.emanuel.temperatureconvertertdd.app.exception.InvalidTemperatureValue;
import com.example.emanuel.temperatureconvertertdd.app.view.MainView;
import com.example.emanuel.temperatureconvertertdd.presenter.MainPresenter;

import javax.inject.Inject;

/**
 * Created by emanuel on 5/01/16.
 */
public class Celsius2FahrenheitWatcher extends ConverterWatcher {

    @Inject
    public Celsius2FahrenheitWatcher(MainPresenter presenter, MainView view) {
        super(presenter, view);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        previous = s.toString();
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if(s.length() == 0 || s.toString().equals("-")){
            value = "";

        }else{
            float floatValue =  convertValueToFloat(s.toString());
            convertValue(floatValue);
        }

    }

    public void convertValue(float value){
        try {

            float convertedValue =presenter.convertToFahrenheit(value);
            this.value = String.valueOf(convertedValue);

        } catch (InvalidTemperatureValue invalidTemperatureValue) {
            view.setCelsius(previous);
            view.showErrorMessage();

        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        view.setFahrenheit(value);
    }
}
