package com.example.emanuel.temperatureconvertertdd.presenter;

import com.example.emanuel.temperatureconvertertdd.app.exception.InvalidTemperatureValue;

/**
 * Created by emanuel on 4/01/16.
 */
public interface MainPresenter {

    float convertToCelsius(float fahrenheit) throws InvalidTemperatureValue;
    float convertToFahrenheit(float celsius) throws InvalidTemperatureValue;
}
