package com.example.emanuel.temperatureconvertertdd.app.exception;

/**
 * Created by emanuel on 5/01/16.
 */
public class InvalidTemperatureValue extends Exception {

    @Override
    public String getMessage() {
        return "you're violating the  thermodynamic's laws";
    }
}
