package com.example.emanuel.temperatureconvertertdd.app.di.modules;

import com.example.emanuel.temperatureconvertertdd.app.activities.TemperatureConverterActivity;
import com.example.emanuel.temperatureconvertertdd.app.view.MainView;
import com.example.emanuel.temperatureconvertertdd.app.view.watchers.Celsius2FahrenheitWatcher;
import com.example.emanuel.temperatureconvertertdd.app.view.watchers.Fahrenheit2CelsiusWatcher;
import com.example.emanuel.temperatureconvertertdd.presenter.MainPresenter;
import com.example.emanuel.temperatureconvertertdd.presenter.MainPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Emanuel on 23/12/2015.
 */
@Module
public class MainModule {

    private TemperatureConverterActivity mainActivity;

    public MainModule(TemperatureConverterActivity activity){
        mainActivity = activity;
    }

    @Provides
    public MainView provideView(){
        return mainActivity;
    }

    @Provides
    public MainPresenter providePresenter(){
        return new MainPresenterImpl();
    }
    @Provides
    public Celsius2FahrenheitWatcher provideCelsius2FahrenheitWatcher(MainPresenter presenter){
        return new Celsius2FahrenheitWatcher(presenter, mainActivity);
    }

    @Provides
    public Fahrenheit2CelsiusWatcher provideFahrenheit2CelsiusWatcher(MainPresenter presenter){
        return new Fahrenheit2CelsiusWatcher(presenter,mainActivity);
    }
}
