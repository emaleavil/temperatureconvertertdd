package com.example.emanuel.temperatureconvertertdd.app.di.modules;

import android.content.Context;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Emanuel on 23/12/2015.
 */
@Module
public class ApplicationModule {
    private Context appContext;

    public ApplicationModule(Context appContext){
        this.appContext = appContext;
    }

    @Provides @Singleton
    public Context provideAppContext(){
        return appContext;
    }

}
