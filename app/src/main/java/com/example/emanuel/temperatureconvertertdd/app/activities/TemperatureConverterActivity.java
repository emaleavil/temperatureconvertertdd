package com.example.emanuel.temperatureconvertertdd.app.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.emanuel.temperatureconvertertdd.R;
import com.example.emanuel.temperatureconvertertdd.app.TemperatureConverterApp;
import com.example.emanuel.temperatureconvertertdd.app.di.components.DaggerMainComponent;
import com.example.emanuel.temperatureconvertertdd.app.di.modules.MainModule;
import com.example.emanuel.temperatureconvertertdd.app.view.MainView;
import com.example.emanuel.temperatureconvertertdd.app.view.watchers.Celsius2FahrenheitWatcher;
import com.example.emanuel.temperatureconvertertdd.app.view.watchers.Fahrenheit2CelsiusWatcher;
import com.example.emanuel.temperatureconvertertdd.presenter.MainPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by emanuel on 16/12/15.
 */
public class TemperatureConverterActivity extends AppCompatActivity implements MainView {

    @Inject
    MainPresenter presenter;

    @Bind(R.id.celsius)
    EditText celsius;
    @Bind(R.id.fahrenheit)
    EditText fahrenheit;

    @Inject
    Celsius2FahrenheitWatcher celsiusWatcher;
    @Inject
    Fahrenheit2CelsiusWatcher fahrenheitWatcher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Adding butterknife view binding
        ButterKnife.bind(this);
        initDagger();

        celsius.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    fahrenheit.removeTextChangedListener(fahrenheitWatcher);
                    celsius.addTextChangedListener(celsiusWatcher);
                }
            }
        });

        fahrenheit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    fahrenheit.addTextChangedListener(fahrenheitWatcher);
                    celsius.removeTextChangedListener(celsiusWatcher);
                }
            }
        });

    }

    public void initDagger(){
        DaggerMainComponent.builder()
                .mainModule(new MainModule(this))
                .applicationComponent(TemperatureConverterApp.getInstance().getComponent())
                .build()
                .inject(this);
    }

    @Override
    public void setFahrenheit(String value) {
        fahrenheit.setText(value);
        fahrenheit.setSelection(fahrenheit.getText().length());
    }

    @Override
    public void setCelsius(String value) {
        celsius.setText(value);
        celsius.setSelection(celsius.getText().length());

    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(this, "Zero absolute!", Toast.LENGTH_LONG).show();
    }
}
