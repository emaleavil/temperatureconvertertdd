package com.example.emanuel.temperatureconvertertdd.app.di.components;

import android.content.Context;

import com.example.emanuel.temperatureconvertertdd.app.di.modules.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Emanuel on 23/12/2015.
 */
@Singleton
@Component(modules =
        {
                ApplicationModule.class
        })
public interface ApplicationComponent {

    void inject(Context context);
}