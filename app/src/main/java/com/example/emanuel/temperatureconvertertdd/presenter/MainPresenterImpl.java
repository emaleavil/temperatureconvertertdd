package com.example.emanuel.temperatureconvertertdd.presenter;

import com.example.emanuel.temperatureconvertertdd.app.exception.InvalidTemperatureValue;

/**
 * Created by emanuel on 4/01/16.
 */
public class MainPresenterImpl implements MainPresenter {

    private final static float ABSOLUTE_ZERO_CELSIUS = -273.15f;
    private final static float ABSOLUTE_ZERO_FAHRENHEIT = -459.67f;

    /**
     * NOTE:
     *
        Presenter shouldn't do the conversion, but to example I don't do another layer.
     *
     */


    @Override
    public float convertToCelsius(float fahrenheit) throws InvalidTemperatureValue {
        if (ABSOLUTE_ZERO_FAHRENHEIT > fahrenheit){
            throw new InvalidTemperatureValue();
        }
        return (fahrenheit - 32) / 1.8f;
    }

    @Override
    public float convertToFahrenheit(float celsius) throws InvalidTemperatureValue {
        if (ABSOLUTE_ZERO_CELSIUS > celsius){
            throw new InvalidTemperatureValue();
        }
        return (celsius * 1.8f) + 32;
    }
}
