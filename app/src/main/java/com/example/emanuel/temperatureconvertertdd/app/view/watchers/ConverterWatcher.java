package com.example.emanuel.temperatureconvertertdd.app.view.watchers;

import android.text.TextWatcher;

import com.example.emanuel.temperatureconvertertdd.app.view.MainView;
import com.example.emanuel.temperatureconvertertdd.presenter.MainPresenter;

/**
 * Created by emanuel on 5/01/16.
 */
public abstract  class ConverterWatcher implements TextWatcher {

    private static final String TAG = ConverterWatcher.class.getName();

    MainPresenter presenter;
    MainView view;

    protected String value;
    protected String previous;

    public ConverterWatcher(MainPresenter presenter, MainView view){
        this.presenter = presenter;
        this.view = view;
    }

    protected float convertValueToFloat(String string){
        float value = Float.valueOf(string);
        return value;
    }

}
