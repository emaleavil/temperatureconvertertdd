package com.example.emanuel.temperatureconvertertdd.app.view.watchers;

import android.text.Editable;

import com.example.emanuel.temperatureconvertertdd.app.exception.InvalidTemperatureValue;
import com.example.emanuel.temperatureconvertertdd.app.view.MainView;
import com.example.emanuel.temperatureconvertertdd.presenter.MainPresenter;

import javax.inject.Inject;

/**
 * Created by emanuel on 5/01/16.
 */
public class Fahrenheit2CelsiusWatcher extends ConverterWatcher {

    @Inject
    public Fahrenheit2CelsiusWatcher(MainPresenter presenter, MainView view) {
        super(presenter, view);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        previous = s.toString();
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if(s.length() == 0 || s.toString().equals("-")){
            value = "";

        }else{
            float floatValue =  convertValueToFloat(s.toString());
            convertValue(floatValue);
        }

    }

    public void convertValue(float value){
        try {

            float convertedValue =presenter.convertToCelsius(value);
            this.value = String.valueOf(convertedValue);

        } catch (InvalidTemperatureValue invalidTemperatureValue) {
            view.setFahrenheit(previous);
            view.showErrorMessage();

        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        view.setCelsius(value);
    }
}
