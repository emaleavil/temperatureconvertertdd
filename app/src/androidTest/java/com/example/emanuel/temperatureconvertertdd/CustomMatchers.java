package com.example.emanuel.temperatureconvertertdd;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * Created by emanuel on 17/12/15.
 */
public class CustomMatchers {

    public static Matcher<View> onScreen(final Activity activity){
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                int[] xy = new int[2];
                item.getLocationOnScreen(xy);

                int[] xyRoot = new int[2];
                activity.getWindow().getDecorView().getLocationOnScreen(xyRoot);

                int y = xy[1] - xyRoot[1];

               return y  >= 0 &&  y <= item.getRootView().getHeight();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("View is not on screen");
            }
        };
    }

    public static Matcher<View> coverAllWidth(){
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                ViewGroup.LayoutParams params = item.getLayoutParams();
                return params.width == ViewGroup.LayoutParams.MATCH_PARENT
                        || params.width == ViewGroup.LayoutParams.FILL_PARENT;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("View must have MATCH_PARENT or FILL_PARENT width");
            }
        };
    }

    public static Matcher<View> isTextSize(final Context context, final float size){
        return new TypeSafeMatcher<View>() {

            private boolean instance = false;

            @Override
            protected boolean matchesSafely(View item) {
                if(!(item instanceof TextView)){
                    instance = true;
                    return false;
                }

                float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
                float sp =   size  * scaledDensity;

                TextView tv = (TextView) item;
                return tv.getTextSize() == sp;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(instance ? "View must be TextView or descendant" : "Text size is not the same");
            }
        };
    }


    public static Matcher<View> isMargin(final Context context, final int size) {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) item.getLayoutParams();

                float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
                float dp =   size  * scaledDensity;

                boolean matches = params.leftMargin == (int) dp;
                matches = matches && params.rightMargin ==  (int) dp;
                matches = matches && params.topMargin ==  (int) dp;
                matches = matches && params.bottomMargin ==  (int) dp;


                return matches;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Margin size doesn't match");
            }
        };
    }

    public static Matcher<View> isEditTextGravity(final int value){
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                EditText editText = (EditText) item;
                int viewGravity = editText.getGravity();
                return viewGravity == value;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Gravity doesn't match");
            }
        };
    }

    public static Matcher<View> availableSpaceForKeyBoard(){
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                int bottom = item.getBottom();
                return bottom > 280;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Is not available space to show the keyboard");
            }
        };
    }

    public static Matcher<View> checkConversion(final float value){
        return new TypeSafeMatcher<View>() {

            @Override
            protected boolean matchesSafely(View item) {
                if(!(item instanceof TextView)) return false;

                float convertedValue = Float.valueOf(((TextView) item).getText().toString());
                float delta = Math.abs(convertedValue - value);

                return delta < 0.005f;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Value expected is wrong");
            }
        };
    }
}
