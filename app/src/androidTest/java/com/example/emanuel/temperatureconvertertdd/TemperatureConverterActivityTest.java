package com.example.emanuel.temperatureconvertertdd;

import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.view.Gravity;

import com.example.emanuel.temperatureconvertertdd.app.activities.TemperatureConverterActivity;
import com.example.emanuel.temperatureconvertertdd.app.exception.InvalidTemperatureValue;
import com.example.emanuel.temperatureconvertertdd.presenter.MainPresenter;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.PositionAssertions.isBelow;
import static android.support.test.espresso.assertion.PositionAssertions.isLeftAlignedWith;
import static android.support.test.espresso.assertion.PositionAssertions.isRightAlignedWith;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.example.emanuel.temperatureconvertertdd.CustomMatchers.availableSpaceForKeyBoard;
import static com.example.emanuel.temperatureconvertertdd.CustomMatchers.checkConversion;
import static com.example.emanuel.temperatureconvertertdd.CustomMatchers.coverAllWidth;
import static com.example.emanuel.temperatureconvertertdd.CustomMatchers.isEditTextGravity;
import static com.example.emanuel.temperatureconvertertdd.CustomMatchers.isMargin;
import static com.example.emanuel.temperatureconvertertdd.CustomMatchers.isTextSize;
import static com.example.emanuel.temperatureconvertertdd.CustomMatchers.onScreen;
import static com.example.emanuel.temperatureconvertertdd.CustomViewActions.setText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by emanuel on 16/12/15.
 */
@RunWith(AndroidJUnit4.class)
public class TemperatureConverterActivityTest extends ActivityInstrumentationTestCase2<TemperatureConverterActivity> {

    private MainPresenter presenter;

    @Rule
    public ActivityTestRule<TemperatureConverterActivity> activityRule = new ActivityTestRule<TemperatureConverterActivity>(TemperatureConverterActivity.class);

    public TemperatureConverterActivityTest() {
        super(TemperatureConverterActivity.class);
    }

    //Use because setUp is not called when runwith annotation is added
    @Before
    public void setUpOwn(){
        presenter = mock(MainPresenter.class);
    }


    @Test
    public final void testPreconditions(){
       assertThat(activityRule, is(notNullValue()));
    }

    @Test
    public final void testHasInputFields(){
       onView(withId(R.id.celsius)).check(matches(notNullValue()));
       onView(withId(R.id.fahrenheit)).check(matches(notNullValue()));

    }

    @Test
    public final void testViewsEmptyAtBeginning(){
        onView(allOf(withId(R.id.celsius),withText(""))).check(matches(isDisplayed()));
        onView(allOf(withId(R.id.fahrenheit),withText(""))).check(matches(isDisplayed()));
    }

    @Test
    public final void testOnScreen(){
        onView(withId(R.id.celsius)).check(matches(onScreen(activityRule.getActivity())));
        onView(withId(R.id.fahrenheit)).check(matches(onScreen(activityRule.getActivity())));
    }

    @Test
    /**
     * If view has padding alignment is true but if you're using margin alignment is false
     */
    public final void testAlignmentWithParent(){
        onView(withId(R.id.celsius_label)).check(isLeftAlignedWith(withId(R.id.parent_container)));
        onView(withId(R.id.celsius)).check(isLeftAlignedWith(withId(R.id.parent_container)));
        onView(withId(R.id.fahrenheit_label)).check(isLeftAlignedWith(withId(R.id.parent_container)));
        onView(withId(R.id.fahrenheit)).check(isLeftAlignedWith(withId(R.id.parent_container)));
    }

    @Test
    public final void testAlignmentWithSiblings(){
        onView(withId(R.id.celsius)).check(isLeftAlignedWith(withId(R.id.celsius_label)));
        onView(withId(R.id.fahrenheit)).check(isLeftAlignedWith(withId(R.id.fahrenheit_label)));
        onView(withId(R.id.fahrenheit)).check(isLeftAlignedWith(withId(R.id.celsius)));
        onView(withId(R.id.fahrenheit)).check(isRightAlignedWith(withId(R.id.celsius)));
    }

    @Test
    public final void testViewPosition(){
        onView(withId(R.id.celsius)).check(isBelow(withId(R.id.celsius_label)));
        onView(withId(R.id.fahrenheit)).check(isBelow(withId(R.id.fahrenheit_label)));
    }

    @Test
    public final void testSiblings(){
        onView(withId(R.id.celsius))
                .check(matches(hasSibling(withId(R.id.celsius_label))))
                .check(matches(hasSibling(withId(R.id.fahrenheit_label))))
                .check(matches(hasSibling(withId(R.id.fahrenheit))));
    }

    @Test
    public final void testCoverAllWidth(){
        onView(withId(R.id.celsius)).check(matches(coverAllWidth()));
        onView(withId(R.id.fahrenheit)).check(matches(coverAllWidth()));
    }

    @Test
    public final void testEditTextSize(){
        onView(withId(R.id.celsius)).check(matches(isTextSize(activityRule.getActivity(),24.0f))); //Custom size
        onView(withId(R.id.celsius_label)).check(matches(isTextSize(activityRule.getActivity(),14.0f))); // Default size for textView
        onView(withId(R.id.fahrenheit)).check(matches(isTextSize(activityRule.getActivity(),18.0f))); //Default size for editText
        onView(withId(R.id.fahrenheit_label)).check(matches(isTextSize(activityRule.getActivity(),14.0f)));
    }

    @Test
    public final void testContainerMargins(){
        onView(withId(R.id.parent_container)).check(matches(isMargin(activityRule.getActivity(), 16)));
    }

    @Test
    public final void testTextGravity(){
        int expected = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
        onView(withId(R.id.celsius)).check(matches(isEditTextGravity(expected)));
        onView(withId(R.id.fahrenheit)).check(matches(isEditTextGravity(expected)));
    }

    @Test
    public final void testKeyboardCanBeShown(){
        onView(withId(R.id.fahrenheit)).check(matches(availableSpaceForKeyBoard()));
    }

    /**
     * This test doesn't check the correct conversion from the passed value
     * Only checks if when we type text in celsius edit text the converted value is setted up on fahrenheit
     * edit text
     */
    @Test
    public final void testFahrenheitToCelsiusConversion() throws InvalidTemperatureValue {
        ViewInteraction celsius = onView(withId(R.id.celsius));
        ViewInteraction fahrenheit = onView(withId(R.id.fahrenheit));

        final float fahrenheitValueToConvert =  32.5f;
        float expected = 90.5f;
        when(presenter.convertToFahrenheit(fahrenheitValueToConvert)).thenReturn(expected);

        celsius.perform(click(),setText(String.valueOf(fahrenheitValueToConvert)));
        fahrenheit.check(matches(checkConversion(expected)));
    }

    @Test
    public final void testCelsiusToFahrenheitConversion() throws InvalidTemperatureValue {
        ViewInteraction celsius = onView(withId(R.id.celsius));
        ViewInteraction fahrenheit = onView(withId(R.id.fahrenheit));

        final float fahrenheitValueToConvert =  90.5f;
        float expected = 32.5f;
        when(presenter.convertToCelsius(fahrenheitValueToConvert)).thenReturn(expected);

        fahrenheit.perform(click(),setText(String.valueOf(fahrenheitValueToConvert))); //click is needed to give focus
        celsius.check(matches(checkConversion(expected)));
    }

    public void tearDown() throws Exception {

    }
}